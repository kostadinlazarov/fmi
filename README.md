# HeleCloud FMI exercise 1 repository

---

This repository is hosting the code that will be used to build the infrastructure for practical exercise 1.

## Useful links

---

Easy way to check from what IP you are reaching Amazon:

* [What Is MyIP - Amazon Checker](http://checkip.amazonaws.com/)

Easy way to check your public IP address:

* [What Is MyIP](http://www.google.com/search?q=what+is+my+ip)

**Notes:**
You should repead those checks in order to test if your LAN is translated to sinble Public IP address or poll of Public IP addresses.

## CloudFormation Template and Parameter files

---

The below table lists all CloudFormation templates and the corresponding parameter files that needs to be deployed.

| Template Name | Parameter File |
| --------------|-------------|
| template.yml | parameters.json |

## CloudFormation Stack Order of Deployment

---

1. Create EC2 Keyp Pair:
    * From AWS management console:

        Detiled step by step instruction could be found in presentation.

    * From AWS CLI:

        **aws ec2 create-key-pair --key-name** _\*Key Pair Name\*_

        _The output is an ASCII version of the key and key fingerprint. You need to save the key to a file._

        **Hint:**

        _For Linux users_

        **aws ec2 create-key-pair --key-name** _\*Key Pair Name\*_ **\>** _\*File Path\*_**.txt**

        _Perform clean up and save it to_ **.pem** _format._

2. Check your Public IP address:
    * _[What Is MyIP - Amazon Checker](http://checkip.amazonaws.com/)_
    * _[What Is MyIP](http://www.google.com/search?q=what+is+my+ip)_
3. Edit parameters in parameters.json file:
    * _add KeyName_
    * _add MyIP_
    * _change any other parameters at your will_
4. Create new CFN stack:
    * From AWS management console:

        _Detiled step by step instruction could be found in presentation._

    * From AWS CLI:

        **aws cloudformation create-stack --stack-name _\*Stack Name\*_ --template-body file://**_\*File Path\*_**/template.yml --parameters file://**_\*File Path\*_**/parameters.json**